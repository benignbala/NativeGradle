use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: hello <name>");
    } else {
        println!("Hello, {}", args[1]);
    }
}

