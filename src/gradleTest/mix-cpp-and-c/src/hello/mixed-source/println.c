#include <stdio.h>

#ifdef __WINDOWS__
#   define EOL "\r\n"
#else
#   define EOL "\n"
#endif

/* Assuming this will be passed a null-terminating string */
void println( const char* s) {
    printf( "%s%s", s, EOL);
}