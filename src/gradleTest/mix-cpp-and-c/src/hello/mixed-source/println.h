#ifndef _EXAMPLE_LIB_WITH_GRADLE_PRINTLN_H
#define _EXAMPLE_LIB_WITH_GRADLE_PRINTLN_H

#ifdef __cplusplus
extern "C" {
#endif

void println( const char* );

#ifdef __cplusplus
}
#endif

#endif
