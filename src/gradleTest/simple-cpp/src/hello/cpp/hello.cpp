#include <iostream>

using std::cout;
using std::endl;

int main(int argc,char** argv) {

    if(argc < 2) {
        cout << "Usage: hello <name>" << endl;
        return 1;
    }

    cout << "Hello, " << argv[1] << endl;
    return 0;
}