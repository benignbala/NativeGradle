#ifndef _EXAMPLE_LIB_WITH_GRADLE_PRINTLN_HPP
#define _EXAMPLE_LIB_WITH_GRADLE_PRINTLN_HPP

#include <ostream>
#include <string>

namespace gradleExamples {

void println( std::ostream&, std::string const& );
void println( std::string const&);

}

#endif
