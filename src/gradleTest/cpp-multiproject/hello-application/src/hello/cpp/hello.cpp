#include <string>
#include <println.hpp>

using gradleExamples::println;
using std::string;

int main(int argc,char** argv) {


    if(argc < 2) {
        println( "Usage: hello <name>" );
        return 1;
    }

    string output("Hello, ");
    output+= argv[1];

    println(output);

    return 0;
}