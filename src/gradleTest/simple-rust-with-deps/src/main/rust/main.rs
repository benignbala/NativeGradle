extern crate md5;
use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: hello <name>");
    } else {
        let digest = md5::compute(args[1].as_bytes());
        let msg = format!("{:x}", digest);
        println!("Hello, {}", args[1]);
        println!("Your hash is {}", msg);
    }
}

