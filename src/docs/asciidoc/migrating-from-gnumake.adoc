== Migrating from GNU Make

The https://plugins.gradle.org/plugin/org.ysb33r.gnumake[GNU Make plugin] can be used to build legacy Make=parts of a codebase. Outputs can be integrated via `model.repositories` or  by using `generatedBy`.

_Example to follow_

////
- Integrating with GnuMake
  - Configuring
  - Inputs / Outputs
  - Using builtBy
////
