= Building Modern Native Applications with Gradle
Schalk W. Cronjé <ysb33r@gmail.com>
:icons: font
:toc: right

== Preface

This has been written as a guide to get people started on their way to using Gradle as the über tool for software construction.

[quote, Schalk Cronjé, Continuous Build and Integration]
____
Classic build tools for native platforms are https://www.gnu.org/software/make/[GMake], http://amake.sourceforge.net/[AMake], including various variants, and http://scons.org/[SCons]. On the JVM the most popular tools are https://ant.apache.org/[Apache Ant], https://maven.apache.org/[Apache Maven], https://gradle.org/[Gradle Build Tool], https://leiningen.org/[Leiningen] and http://www.scala-sbt.org/[ScalaBuild Tool]. People working on the Windows operating system and mostly using .NET will be familiar with https://github.com/microsoft/msbuild[MSBuild] and some extent link:http://www.mono-project.com/docs/tools+libraries/tools/xbuild/[XBuild]. For JavaScript, there is https://gruntjs.com/[Grunt] and http://gulpjs.com/[Gulp] as the most popular choices. Some programming languages even have their own build tools, which are very language-specific and not really suited to building anything else. This includes Go and Rust. There are even tools which specialise in massive parallel builds such as https://bazel.build/[Basel].
____

Gradle is an powerful build-tool with a small functionality footprint, which is then extended via plugins. This means that the build script authors are not overburdened with unnecessary build tasks, only the ones that is applicable to the kind of projects they want to build are made available. It leads to less cruft and less confusion.

include::the-modern-build-tool.adoc[]

include::before-you-start.adoc[]

include::cpp-simple-executable.adoc[]

include::cpp-simple-library.adoc[]

include::cpp-multi-project.adoc[]

include::cpp-library-tested.adoc[]

include::mixing-cpp-and-c.adoc[]

// include::multi-platform-cpp.adoc[]

// include::documentation-for-cpp.adoc[]

include::working-with-external-tools.adoc[]

// include::migrating-from-gnumake.adoc[]

// include::integrating-cmake.adoc[]

// include::integrating-conan.adoc[]

// include::integrating-conan.adoc[]

include::simple-rust-executable.adoc[]

// include::simple-rust-library.adoc[]

// include::startng-with-go.adoc[]

// including::starting-with-d.adoc[]

// Mix and match languages ???


include::contributions.adoc[]

include::books.adoc[]

