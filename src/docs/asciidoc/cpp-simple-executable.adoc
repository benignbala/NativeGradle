== Simple C++ Executable

Gradle has built-in support for C++. It has extensive, although sometimes esoteric, DSL to describe the srouces, platforms and build flavours. This DSL is being reworked and we hope to see something more streamline in the coming Gradle 5.x family.

=== Conventional layout

Gradle uses conventions to simplify build specifications and as such it adheres to the conventions-over-configuration principle. By default it follows a layour that people in the JVM world will be more used to.

include::cpp/simple-cpp-layout.adoc[]

In case you were wonderign, Gradle is flexible enough to allow other layouts, but that just means specifying it in the build script.

Start with a simple C++ hello world type of program using the above layout. Create a directory for your example project and then inside your project directory create a `src.hello/cpp/hello.cpp` file.

.src/hello/cpp/hello.cpp
[source,cpp]
----
include::{examples}/simple-cpp/src/hello/cpp/hello.cpp[]
----

=== Create your C++ project

Now add a build script called `build.gradle` in the project directory.

.build.gradle
[source,groovy]
----
include::{examples}/simple-cpp/build.gradle[]
----
<1> The plugins block allows for sepcific behaviour to be introduced into the build. As this is a C++ project, the built-in `cpp` plugin is applied.
<2> All C, C++, Objective C & assembler projects are described inside a `model` block.
<3> The final artifact is an executable and therefore the predefined keyword `NativeExecutableSpec` is used. Note that the name of the project is `hello` which will then know to look for source in a `src/hello` folder. Because the `cpp` plugin has been applied it will look for sources in `src/hello/cpp`.

The behaviour introduced by the `cpp` plugin includes the creation of a number of tasks related to the project that will be built.

include::cpp/hello-tasks.adoc[]

====
Even at this early stage, users of Make flavours should be able to see that a lot less specification is required to build a basic project. CMake users may think that this is also taken care of by CMake, but they should probably recognise that the build script definition is a s cryptic than that of CMake.
====

Now build the code by usiing the over-arching lifecycle task called, `build`.

[listing]
----
$ ./gradlew build
----

=== Layout after building

Have a look at how the directory layout has changed.

include::cpp/build-layout-for-exe.adoc[]

====
*When things go wrong*

If your compilation or linkage fails you'll see a message such as below.


[listing]
----
* What went wrong:
Execution failed for task ':hello-application:compileHelloExecutableHelloCpp'.
> A build operation failed.
      C++ compiler failed while compiling hello.cpp.
  See the complete log at: file:///..../build/tmp/compileHelloExecutableHelloCpp/output.txt
----

It will provide you with the location of the appropriate output which you can then investigate. In this way your console output is spared unnecessary noise. You also do not have to back-scroll to find the error, but rather browse it at your ease from a text editor.
====

You can now run

[listing]
----
$ ./gradlew installHelloExecutable

$ ./build/install/hello/hello

Usage: hello <name>
----

and admire the craft of your first C++ project with Gradle.

