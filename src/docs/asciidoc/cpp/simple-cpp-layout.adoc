[listing]
----
.
├── build.gradle
├── settings.gradle
└── src
    └── hello    <1>
        └── cpp  <2>
            └── hello.cpp
----
<1> The name of the executable is determined by the name of the project, which is also linekd to the name of the folder in which the code is placed, Here you are building a executable called `hello` (`hello.exe`) and you will place the source in a `src/hello` folder by convention.
<2> All C++ source code goes in to a `cpp` subfolder below the named folder.
