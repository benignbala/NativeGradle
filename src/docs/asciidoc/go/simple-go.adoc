
.go/src/hello/helloworld.go
[source,go]
----
include::{examples}/simple-go/go/src/hello/helloworld.go[]
----

.build.gradle
[source,groovy]
----
include::{examples}/simple-go/build.gradle[tags=go-plugin]
----
